//
//  Item1.swift
//  Actividad1
//
//  Created by Iván Gálvez Rochel on 15/3/17.
//  Copyright © 2017 Iván Gálvez Rochel. All rights reserved.
//

import UIKit

class Item1: UIViewController, UITableViewDelegate, UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 25
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let Protocelda:item1_celda = tableView.dequeueReusableCell(withIdentifier: "ProtoCelda", for: indexPath) as! item1_celda
        Protocelda.lbl?.text = String(format: "Celda %d", indexPath.row)
        Protocelda.img?.image = #imageLiteral(resourceName: "AWESOME_FACE!!!.png")
        return Protocelda
    }

}
