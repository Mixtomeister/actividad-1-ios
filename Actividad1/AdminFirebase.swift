//
//  DataHolder.swift
//  Actividad1
//
//  Created by Iván Gálvez Rochel on 21/3/17.
//  Copyright © 2017 Iván Gálvez Rochel. All rights reserved.
//

import UIKit
import Firebase

class AdminFirebase: NSObject {
    static let instancia: AdminFirebase = AdminFirebase()

 
    func configFirebase(){
        FIRApp.configure()
    }
    
    func crearUsuario(email : String, pass : String, delegate: AdminFirebaseDelegate){
        FIRAuth.auth()?.createUser(withEmail: email, password: pass) { (user, error) in
            if error != nil {
                delegate.creacionUsuario!(err: -1)
            }else{
                delegate.creacionUsuario!(err: 0)
            }
        }
    }
    
    func iniciarSesion(email : String, pass : String, delegate: AdminFirebaseDelegate){
        FIRAuth.auth()?.signIn(withEmail: email, password: pass) { (user, error) in
            if error != nil {
                delegate.inicioSesion!(err: -1)
            }else{
                delegate.inicioSesion!(err: 0)
            }
        }
    }
    
    func singOut(delegate: AdminFirebaseDelegate){
        try! FIRAuth.auth()!.signOut()
        delegate.cierreSesion!()
    }
    
    func borrarUsuario(delegate: AdminFirebaseDelegate){
        FIRAuth.auth()?.currentUser?.delete()
        delegate.cierreSesion!()
    }
}

@objc protocol AdminFirebaseDelegate{
    @objc optional func creacionUsuario(err:Int)
    @objc optional func inicioSesion(err:Int)
    @objc optional func cierreSesion()
}
