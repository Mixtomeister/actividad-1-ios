//
//  more.swift
//  Actividad1
//
//  Created by Iván Gálvez Rochel on 21/3/17.
//  Copyright © 2017 Iván Gálvez Rochel. All rights reserved.
//

import UIKit

class more: UIViewController, AdminFirebaseDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func singOut() {
        AdminFirebase.instancia.singOut(delegate: self)
    }
    
    func cierreSesion() {
        self.performSegue(withIdentifier: "backToMain", sender: self)
    }
    
    @IBAction func borrarUsuario(){
        AdminFirebase.instancia.borrarUsuario(delegate: self)
    }
}
